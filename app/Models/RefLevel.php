<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RefLevel
 * @package App\Models
 * @version September 30, 2020, 4:22 am UTC
 *
 * @property string $name
 * @property string $description
 * @property string $rewards
 * @property string $congratulatory_message
 * @property string $terget_no_refferals
 * @property string $point_per_refferal
 */
class RefLevel extends Model
{
    use SoftDeletes;

    public $table = 'ref_levels';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'description',
        'rewards',
        'congratulatory_message',
        'terget_no_refferals',
        'point_per_refferal'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'rewards' => 'string',
        'congratulatory_message' => 'string',
        'terget_no_refferals' => 'string',
        'point_per_refferal' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|max:255',
        'description' => 'nullable|string',
        'rewards' => 'nullable|string|max:255',
        'congratulatory_message' => 'nullable|string|max:255',
        'terget_no_refferals' => 'nullable|string|max:255',
        'point_per_refferal' => 'nullable|string|max:255',
        'deleted_at' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    
}
