<?php

namespace App\Repositories;

use App\Models\RefLevel;
use App\Repositories\BaseRepository;

/**
 * Class RefLevelRepository
 * @package App\Repositories
 * @version September 30, 2020, 4:22 am UTC
*/

class RefLevelRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description',
        'rewards',
        'congratulatory_message',
        'terget_no_refferals',
        'point_per_refferal'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RefLevel::class;
    }
}
