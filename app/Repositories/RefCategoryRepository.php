<?php

namespace App\Repositories;

use App\Models\RefCategory;
use App\Repositories\BaseRepository;

/**
 * Class RefCategoryRepository
 * @package App\Repositories
 * @version September 30, 2020, 4:22 am UTC
*/

class RefCategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RefCategory::class;
    }
}
