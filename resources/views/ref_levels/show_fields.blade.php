<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $refLevel->name }}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $refLevel->description }}</p>
</div>

<!-- Rewards Field -->
<div class="form-group">
    {!! Form::label('rewards', 'Rewards:') !!}
    <p>{{ $refLevel->rewards }}</p>
</div>

<!-- Congratulatory Message Field -->
<div class="form-group">
    {!! Form::label('congratulatory_message', 'Congratulatory Message:') !!}
    <p>{{ $refLevel->congratulatory_message }}</p>
</div>

<!-- Terget No Refferals Field -->
<div class="form-group">
    {!! Form::label('terget_no_refferals', 'Terget No Refferals:') !!}
    <p>{{ $refLevel->terget_no_refferals }}</p>
</div>

<!-- Point Per Refferal Field -->
<div class="form-group">
    {!! Form::label('point_per_refferal', 'Point Per Refferal:') !!}
    <p>{{ $refLevel->point_per_refferal }}</p>
</div>

