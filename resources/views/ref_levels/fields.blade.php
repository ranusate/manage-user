<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Rewards Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rewards', 'Rewards:') !!}
    {!! Form::text('rewards', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Congratulatory Message Field -->
<div class="form-group col-sm-6">
    {!! Form::label('congratulatory_message', 'Congratulatory Message:') !!}
    {!! Form::text('congratulatory_message', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Terget No Refferals Field -->
<div class="form-group col-sm-6">
    {!! Form::label('terget_no_refferals', 'Terget No Refferals:') !!}
    {!! Form::text('terget_no_refferals', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Point Per Refferal Field -->
<div class="form-group col-sm-6">
    {!! Form::label('point_per_refferal', 'Point Per Refferal:') !!}
    {!! Form::text('point_per_refferal', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('refLevels.index') }}" class="btn btn-default">Cancel</a>
</div>
